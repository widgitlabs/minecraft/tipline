/**
 *    This file is part of TipLine.
 *
 *    TipLine is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    TipLine is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with TipLine.
 *    If not, see <http://www.gnu.org/licenses/>.
 */

package com.widgetinteractive.tipline.common;

import java.util.Arrays;
import java.util.List;

import com.widgetinteractive.tipline.TipLine;
import com.widgetinteractive.tipline.common.ConfigHandler;
import com.widgetinteractive.tipline.common.util.TipLineUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiDownloadTerrain;
import net.minecraft.client.gui.GuiScreenWorking;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = TipLine.MODID)
public class EventHandler {
	public static String tip;
	public static Integer tipIndex;
	private static List<String> tipList;
	
	@SubscribeEvent
	public static void onGuiScreenEventInitGuiEvent(GuiScreenEvent.InitGuiEvent event) {
		tipList = Arrays.asList(ConfigHandler._tipList);
		
		if (tipList.size() > 0) {
			tip = tipList.get(TipLine.random.nextInt(tipList.size()));
		} else {
			tip = "Something went wrong!";
		}
		
		tipIndex = tipList.indexOf(tip);
	}
	
	@SubscribeEvent
	public static void onGuiScreenEventDrawScreenEvent(GuiScreenEvent.DrawScreenEvent event) {
		if (event.getGui() instanceof GuiDownloadTerrain || event.getGui() instanceof GuiScreenWorking) {
			TipLineUtils.drawTips(Minecraft.getMinecraft().fontRenderer);
		}
	}
}