/**
 *    This file is part of TipLine.
 *
 *    TipLine is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    TipLine is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with TipLine.
 *    If not, see <http://www.gnu.org/licenses/>.
 */

package com.widgetinteractive.tipline.common;

import net.minecraft.util.text.TextFormatting;

public final class DisplayHandler {
	public enum Position {
		TOPLEFT,
		TOPCENTER,
		TOPRIGHT,
		BOTTOMLEFT,
		BOTTOMCENTER,
		BOTTOMRIGHT
	}
	
	public enum Color {
		BLACK,
		DARK_BLUE,
		DARK_GREEN,
		DARK_RED,
		DARK_PURPLE,
		GOLD,
		GRAY,
		DARK_GRAY,
		BLUE,
		GREEN,
		AQUA,
		RED,
		LIGHT_PURPLE,
		YELLOW,
		WHITE
	}

	public static String getColor(String color) {
		switch (color) {
			case "BLACK" :
				color = TextFormatting.BLACK.toString();
				break;
			case "DARK_BLUE" :
				color = TextFormatting.DARK_BLUE.toString();
				break;
			case "DARK_GREEN" :
				color = TextFormatting.DARK_GREEN.toString();
				break;
			case "DARK_RED" :
				color = TextFormatting.DARK_RED.toString();
				break;
			case "DARK_PURPLE" :
				color = TextFormatting.DARK_PURPLE.toString();
				break;
			case "GOLD" :
				color = TextFormatting.GOLD.toString();
				break;
			case "GRAY" :
				color = TextFormatting.GRAY.toString();
				break;
			case "DARK_GRAY" :
				color = TextFormatting.DARK_GRAY.toString();
				break;
			case "BLUE" :
				color = TextFormatting.BLUE.toString();
				break;
			case "GREEN" :
				color = TextFormatting.GREEN.toString();
				break;
			case "AQUA" :
				color = TextFormatting.AQUA.toString();
				break;
			case "RED" :
				color = TextFormatting.RED.toString();
				break;
			case "LIGHT_PURPLE" :
				color = TextFormatting.LIGHT_PURPLE.toString();
				break;
			case "YELLOW" :
				color = TextFormatting.YELLOW.toString();
				break;
			case "WHITE" :
				color = TextFormatting.WHITE.toString();
				break;
		}
		
		return color;
	}
}