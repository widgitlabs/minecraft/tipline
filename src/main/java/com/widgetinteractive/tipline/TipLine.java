/**
 *    This file is part of TipLine.
 *
 *    TipLine is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    TipLine is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with TipLine.
 *    If not, see <http://www.gnu.org/licenses/>.
 */

package com.widgetinteractive.tipline;

import java.util.Random;

import org.apache.logging.log4j.Logger;

import com.widgetinteractive.tipline.proxy.CommonProxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = TipLine.MODID, name = TipLine.MODNAME, version = TipLine.MODVER, acceptedMinecraftVersions = "[1.12]")
public class TipLine {
	public static final String MODID = "tipline";
	public static final String MODNAME = "TipLine";
	public static final String MODVER = "1.0.4";
	
	@SidedProxy(clientSide = "com.widgetinteractive.tipline.proxy.ClientProxy", serverSide = "com.widgetinteractive.tipline.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@Mod.Instance
	public static TipLine instance;
	
	public static Logger logger;
	public static final Random random = new Random();
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		proxy.preInit(event);
	}
}